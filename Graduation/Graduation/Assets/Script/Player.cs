﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviour {

    // パラメーター
    public enum Parameters
    {
        Power = 0,
        Luck,
        Speed
    }

    public GameObject goEnemy;    // 敵
    GameObject goEffect;          // エフェクト
    
    bool bIsEnemyCollision = false;  // 敵に衝突していたらtrue
    float fPlayerSpeed = 0.01f;  // プレイヤーの速度
    Vector3 vPlayerStartPos;     // プレイヤーの初期座標

	// Use this for initialization
	void Start () {
        // プレイヤーの初期座標をビューポート座標に変換
        vPlayerStartPos = Camera.main.WorldToViewportPoint(this.gameObject.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
        // 移動
        EnemyMove();
        //PlayerAttackDamage();
    }

    // 移動
    void EnemyMove()
    {
        // 敵に当たった時
        if (bIsEnemyCollision)
        {
            IsHitEnemyMove();
        }

        // 敵に当たっていない時
        else
        {
            // プレイヤーの座標をビューポート座標に変換
            Vector3 vPlayerViewportPos = Camera.main.WorldToViewportPoint(this.gameObject.transform.position);
            
            // プレイヤーの攻撃間隔を取得
            float fPlayerInterval = GetPlayerInterval();
            vPlayerViewportPos.x += fPlayerInterval;
            this.gameObject.transform.position = Camera.main.ViewportToWorldPoint(vPlayerViewportPos);
        }
    }

    // Collision2Dが呼ばれていたら
    void OnCollisionEnter2D(Collision2D col)
    {
        // 敵に衝突していたら
        if(col.gameObject.tag == "Enemy"){
            bIsEnemyCollision = true;
        }
    }

    // 敵に当たった後の移動
    void IsHitEnemyMove()
    {
        // プレイヤーの座標をビューポート座標に変換
        Vector3 vPlayerViewportPos = Camera.main.WorldToViewportPoint(this.gameObject.transform.position);
        if (vPlayerViewportPos.x < vPlayerStartPos.x)
        {
            vPlayerViewportPos.x = vPlayerStartPos.x;
            this.gameObject.transform.position = Camera.main.ViewportToWorldPoint(vPlayerViewportPos);
            bIsEnemyCollision = false;
        }
        else
        {
            // 加速度を計算した値を代入
            vPlayerViewportPos.x += -GetAcceleration();
            this.gameObject.transform.position = Camera.main.ViewportToWorldPoint(vPlayerViewportPos);
            
        }
    }

    // 加速度計算
    float GetAcceleration()
    {
        // 速度を取得
        float fInterval = (float)(GetStatus((int)Parameters.Speed));

        float Speed = fPlayerSpeed;    // 速度
        const float Acceleration = 0.005f;  // 加速度

        // 倍率
        const float fMagnification = 0.0001f;
        Speed += Acceleration + (fInterval * fMagnification);
        return Speed;
    }

    

    // 攻撃力計算
    public float GetPlayerAttack()
    {
        // 攻撃のステータスを取得
        float fPower = GetStatus((int)Parameters.Power);
        // 基礎攻撃力
        float fBaseAttack = 10;
        // 最終攻撃力
        float fAttack = fBaseAttack + (fPower * 0.5f);

        return fAttack;
    }

    // 会心確率計算
    public float GetPlayerCritical()
    {
        // 運を取得
        float fLuck = (float)(GetStatus((int)Parameters.Luck));

        // 基礎値
        float fBaseLuck = 0.1f;

        // 倍率
        const float fMagnification = 0.001f;

        // 最終的
        float fCritical = fBaseLuck + (fLuck * fMagnification);

        return fCritical;
    }

    // 攻撃間隔計算
    public float GetPlayerInterval()
    {
        // 攻撃間隔を取得
        float fInterval = (float)(GetStatus((int)Parameters.Speed));

        // 基礎値
        float fBaseInterval = fPlayerSpeed;

        // 倍率
        const float fMagnification = 0.0001f;

        // 最終攻撃間隔
        float fSpeed = fBaseInterval + (fInterval * fMagnification);

        return fSpeed;
    }


    // ステータス取得
    float GetStatus( int Parameters )
    {
        // ステータスのオブジェクトを取得
        var Power = GameObject.Find("Power");
        var Luck = GameObject.Find("Luck");
        var Speed = GameObject.Find("Speed");

        // ステータス(この時点ではText)を取得
        var PowerText = Power.GetComponent<Text>().text;
        var LuckText = Luck.GetComponent<Text>().text;
        var SpeedText = Speed.GetComponent<Text>().text;

        float[] Status = { float.Parse(PowerText), float.Parse(LuckText), float.Parse(SpeedText) };
        return Status[Parameters];
    }

}
