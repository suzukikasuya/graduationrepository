﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResPawn : MonoBehaviour {

    public GameObject goEnemy;
    Vector3 vEnemyPos;
    GameObject goCan;
    GameObject goPlayer;
    Vector3 vPlayerPos;
    int iHp;
    public Sprite[] goEnemyTextureArray;    // 敵のテクスチャをまとめる配列

	void Start () {
        SetGameObject();
        vPlayerPos = goPlayer.transform.position;
        vEnemyPos = goEnemy.transform.position;
	}
    //Enemyのリスポーン関数、同時にEnemyのHP上限を増やす
    public void EnemyResPawn(int HpUp){
        GameObject obj = Instantiate(goEnemy, vEnemyPos, Quaternion.identity) as GameObject;
        obj.transform.SetParent(goCan.transform, false);
        iHp += HpUp;
        obj.GetComponent<Enemy>().iEnemyHPMAX += iHp;
        obj.GetComponent<Image>().sprite = goEnemyTextureArray[Random.Range(0,goEnemyTextureArray.Length)];
        obj.GetComponent<Enemy>().Start();
    }
    //Playerのリスポーン関数
    //-1しているのはステータスポイントが０に戻るバグを防ぐため
    public void PlayerResPawn(){
        goPlayer.transform.position = new Vector3(vPlayerPos.x - 1, vPlayerPos.y, vPlayerPos.z);
    }
    //GameObject型の初期化
    void SetGameObject(){
        goPlayer = GameObject.Find("Player");
        goCan = GameObject.Find("Canvas");
    }
}
