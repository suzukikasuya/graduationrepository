﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Enemy : MonoBehaviour {

    public GameObject[] goEffectBox;    // エフェクトをまとめる配列
    public GameObject goPlayer;         // プレイヤー
    private GameObject goCanvas;
    GameObject goHitPointBar;           //HPバー
    public int iEnemyHPMAX;    //敵の最大体力
    int iEnemyHPNow;    //現在のＨＰ
    Vector3 vHPBarLength;   //HPバーの長さ
    GameObject HPText;
    GameObject goStatusPoint;
    int iStatusPoint;
    public GameObject goDamegeText;    // ダメージ表示
    
    

    // エフェクトの種類
    public enum EffectList
    {
        SlashEffect = 0,
        CriticalEffect,
        EffectEnd
    }

    public void Start () {
        //HPの初期化
        iEnemyHPNow = iEnemyHPMAX;
        SetGameObject();
        SetHPBar(5);
    }

	void Update () {
        iStatusPoint = int.Parse(goStatusPoint.GetComponent<Text>().text);
        HPText.GetComponent<Text>().text = iEnemyHPNow + "/" + iEnemyHPMAX;
	}

    // Collision2Dが呼ばれていたら
    void OnCollisionEnter2D(Collision2D col)
    {
        // 敵に衝突していたら
        if (col.gameObject.tag == "Player")
        {
            // エフェクトの種類をランダムで決める
            float fRandomEffect = Random.Range(0.0f, 1.0f);
            
            // Luckを取得
            float fPlayerLuck = goPlayer.GetComponent<Player>().GetPlayerCritical();
            // クリティカルの確率
            float fCritical = fPlayerLuck;
            // 生成するエフェクトの種類
            int iEffectType = (int)EffectList.SlashEffect;

            if (fRandomEffect < fCritical)
            {
                iEffectType = (int)EffectList.CriticalEffect;
            }

            // エフェクトの種類を指定
            EffectVariation(iEffectType);
        }  
    }

    // エフェクトの種類を設定
    void EffectVariation( int iEffect )
    {
        // RectTransformの座標を取得
        var vRectTransform = this.gameObject.GetComponent<RectTransform>().anchoredPosition;
        
        // エフェクト生成
        var goEffect = Instantiate(goEffectBox[iEffect], vRectTransform, Quaternion.identity) as GameObject; // as ←キャストと同じ
        // 親をCanvasに設定
        goEffect.transform.SetParent(goCanvas.transform, false);

        // エフェクトの種類によってダメージを変更
        EnemyDamage(iEffect);
    }

    // ダメージ計算
    void EnemyDamage( int iEffect )
    {
        // 攻撃の種類によって与えるダメージを設定する配列
        float[] iEffectDamageArray = { 1.1f, 1.5f };

        // プレイヤーの攻撃力を取得
        float fPlayerAttack = goPlayer.GetComponent<Player>().GetPlayerAttack();

        // ダメージを設定
        int iEnemyDamage = (int)(fPlayerAttack * iEffectDamageArray[iEffect]);
        HitPointBar(iEnemyDamage, iEnemyHPNow);
        iEnemyHPNow -= iEnemyDamage;
        EnemyDamegeDisplay(iEnemyDamage ,iEffect);
    
        if (iEnemyHPNow <= 0)
        {
            goHitPointBar.GetComponent<RectTransform>().localScale = new Vector3(0.0f, 0.5f, 1.0f);
            iEnemyHPNow = 0;
            HPText.GetComponent<Text>().text = iEnemyHPNow + "/" + iEnemyHPMAX;
            iStatusPoint += 3;
            goStatusPoint.GetComponent<Text>().text = iStatusPoint + "";    
            Destroy(this.gameObject);
        }
    }

    // ダメージ表示
    void EnemyDamegeDisplay( int iDamege, int iEffect )
    {
        Color[] DamegeColor = { 
            Color.white,
            Color.red
        };
        GameObject DamegeText = Instantiate(goDamegeText, this.gameObject.transform.position, Quaternion.identity) as GameObject;
        DamegeText.GetComponent<Text>().color = DamegeColor[iEffect];
        DamegeText.transform.SetParent(this.gameObject.transform, false);
        DamegeText.GetComponent<Text>().text = iDamege + "";
    }

    //HPバーのやつ
    void HitPointBar(float fDamage, float fHPnow){
        //現在のHPが０より高かったら
        if (iEnemyHPNow > 0){
            //ダメージを受けて全体の何％のＨＰになったか
            float a = (fHPnow - fDamage) / iEnemyHPMAX;
            //描画処理
            goHitPointBar.GetComponent<RectTransform>().localScale = new Vector3(vHPBarLength.x * a, 0.5f, 1.0f);
        } 
    }
    //ＨＰバーの初期化
    void SetHPBar(int iBarLength)
    {
        vHPBarLength = goHitPointBar.GetComponent<RectTransform>().localScale;
        vHPBarLength.x = iBarLength;
        goHitPointBar.GetComponent<RectTransform>().localScale = vHPBarLength;
    }
    //GameObject型の変数の初期化
    void SetGameObject()
    {
        goCanvas = GameObject.Find("Canvas");
        goHitPointBar = GameObject.Find("HPBar");
        HPText = GameObject.Find("HPText");
        goPlayer = GameObject.Find("Player");
        goStatusPoint = GameObject.Find("StatusPoint");
        
    }
}
