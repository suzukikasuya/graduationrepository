﻿using UnityEngine;
using System.Collections;


public class Effect : MonoBehaviour {

    GameObject goEnemy; // 敵
    bool DestroyEffectFlg = false;  // エフェクトが消える時true

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // エフェクトが消える
        if(DestroyEffectFlg){
            DestroyEffect();
        }
        
	}

    // エフェクトを消す
    public void DestroyEffect()
    {
        Destroy(this.gameObject);
    }
        
}
