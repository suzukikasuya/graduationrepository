﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour {
    public GameObject goFade;
    GameObject goCan;
    GameObject goFadeManager;
    bool bInstanFlg = true;
    GameObject goResPawn;
    void Start () {
        SetGameObject();
    }
	void Update () {
        NoEnemy(GameObject.FindGameObjectWithTag("Enemy"));    
    }
    //Enemyがいるかいないか,いなかったらfadeを生成関数を呼ぶ
    void NoEnemy(GameObject Enemy){
        bool b = !Enemy && bInstanFlg;
        if (b){
            InstanFade();    
        }
    }
    //Fade生成
    //Animation Eventの関係上,生成されるFadeObjectにも同じスクリプトが入っているためObject名で判別
    //また、生成し続けるのを防ぐためflgで管理
    void InstanFade(){
        if (this.gameObject.name == "FadeManager"){
            GameObject obj = Instantiate(goFade);
            obj.transform.SetParent(goCan.transform, false);
        }
        bInstanFlg = false;
    }
    //デストロイ関数
    public void Destroy(){
        ResPawnObject();
        Destroy(this.gameObject);
        goFadeManager.GetComponent<Fade>().bInstanFlg = true;
    }
    //Player,Enemyをリスポーンさせる関数
    void ResPawnObject(){
        goResPawn.GetComponent<ResPawn>().EnemyResPawn(100);
        goResPawn.GetComponent<ResPawn>().PlayerResPawn();
    }
    //GameObject型の変数の初期化
    void SetGameObject(){
        goResPawn = GameObject.Find("ResPawnManager");
        goFadeManager = GameObject.Find("FadeManager");
        goCan = GameObject.Find("Canvas");   
    }
}
