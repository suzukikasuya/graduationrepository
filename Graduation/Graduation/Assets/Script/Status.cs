﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Status : MonoBehaviour {
    int ParamaterMax = 999;
    GameObject goStetusPoint;
    
    //ステータスポイントの値
    string strStetusPoint;
    //float型に変換
    float fStetusPoint;
    //ステータスの値
    string strText; 
    //float型に変換
    float TextParse;
    //
    bool LongPushFlg;
    //
    float fDownTime = 0;
    void Start(){
        goStetusPoint = GameObject.Find("StatusPoint");
    }

    void Update(){
        //押されてる間
        if (LongPushFlg){
            fDownTime += Time.deltaTime;
            Debug.Log(fDownTime);
        }
        //押されてる時間が2.0fだったら
        if (fDownTime > 2.0f){
            //各種値を習得
            strStetusPoint = goStetusPoint.GetComponent<Text>().text;
            fStetusPoint = float.Parse(strStetusPoint);
            strText = this.gameObject.GetComponent<Text>().text;
            TextParse = float.Parse(strText);
            //全部ぶちこむ
            TextParse += fStetusPoint;
            fStetusPoint = 0;
            if(TextParse > 999){
                float ovar = TextParse - 999;
                TextParse = 999;
                fStetusPoint = ovar;
            }
            
            fDownTime = 0;
            StetusView();
            LongPushFlg = false;
        }

    }
    //ステータス管理
    public void SetState(bool b){
        //各種値を習得
        strStetusPoint = goStetusPoint.GetComponent<Text>().text;
        fStetusPoint = float.Parse(strStetusPoint);
        strText = this.gameObject.GetComponent<Text>().text;
        TextParse = float.Parse(strText);
        bool tp_ZERO = TextParse == 1 && !b;
        bool sp_ZERO = fStetusPoint == 0 && b;
        //マイナスいかない
        if (tp_ZERO){
            return;
        }
        //プラス超えない
        else if (sp_ZERO){
            return;
        }
        //プラス、マイナスどちらが押されたか
        if (b){
            //カンストの処理
            if (TextParse != ParamaterMax){
                StetusChange(1);
            }
        }
        else{
            StetusChange(-1);
        }
    }

    //ステータスの増減
    void StetusChange(int i){
        //ポイントが0よりあれば
        if (fStetusPoint > 0){
            //ステータスポイントの増減
            fStetusPoint -= i;
            //値を増減
            TextParse += i;
        }
        //0未満なら
        else{
            fStetusPoint++;
            TextParse--;
        }      
        StetusView();
    }

    //長押し　押されてる間
    public void ButtonDown(bool b)
    {
        LongPushFlg = true;
    }
    //長押し　離されたら
    public void ButtonUp(bool b)
    {
        LongPushFlg = false;
        fDownTime = 0;    
    }


    //描画処理
    void StetusView(){
        goStetusPoint.GetComponent<Text>().text = fStetusPoint + "";
        this.gameObject.GetComponent<Text>().text = TextParse + "";
    }
}
